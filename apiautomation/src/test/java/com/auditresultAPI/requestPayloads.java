package com.auditresultAPI;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.utility.readProperties;
import com.workbenchflowAPI.getReportDetails;
import com.workbenchflowAPI.getReportNumberList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class requestPayloads {

    static String payload = null;
    static Map<String, String> PropertiescLass = new HashMap<String, String>();
    static getReportNumberList reportlist = new getReportNumberList();


    public static String submissionDatefilter() {
        payload = "{\"to_submission_date\":\"2020-05-21T01:12:00\",\"from_submission_date\":\"2020-06-04T01:04:00\"}";
        return payload;
    }

    public static String reportNOTINfilter() {
        Response WorkbenchAPI = getReportDetails.retrieveReportParameterFromFirstReport(TestClass.ReportNumberList.get(0));
        JsonPath jpath_workbench = new JsonPath(WorkbenchAPI.asString());
        String reportNumber = jpath_workbench.getJsonObject("data.expense_report.external_report_id");
        payload = "{\"report_id_not_in\":[\"" + reportNumber + "\"]}";
        return payload;

    }

    public static String auditDatefilter() {
        payload = "{\"from_audit_date\":\"2020-06-21T01:12:00\",\"to_audit_date\":\"2020-06-23T01:04:00\"}";
        return payload;
    }

    public static String reportINfilter(String testCase) throws IOException {


        if (testCase.equals("ReportIDin") || testCase.equals("LineLevelAuditDetail")) {
            //Workbench API
            Response WorkbenchAPI = getReportDetails.retrieveReportParameterFromFirstReport(TestClass.ReportNumberList.get(0));
            JsonPath jpath_workbench = new JsonPath(WorkbenchAPI.asString());
            String reportNumber = jpath_workbench.getJsonObject("data.expense_report.external_report_id");
            payload = "{\"report_id_in\":[\"" + reportNumber + "\"]}";
        } else if (testCase.equals("WrongReportID")) {
            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            Object wrong_ReportID = PropertiescLass.get("wrong_ReportID");
            payload = "{\"report_id_in\":[\"" + wrong_ReportID + "\"]}";
        }

        return payload;
    }

    public static String sortDirection() {

        payload = "{\"sort_direction\":\"DESC\"}";
        return payload;
    }

    public static String sortField() {
        payload = "{\"sort_field\" :\"external_report_id\"}";
        return payload;
    }

    public static String pageNumber() {
        payload = "{\"page_number\" :\"6\"}";
        return payload;
    }

    public static String pageSize() {
        payload = "{\"page_size\" :\"6\"}";
        return payload;
    }

/*
    public static String customerID() throws IOException {

        PropertiescLass = readProperties.readProperties();
        Object wrong_ReportID = PropertiescLass.get("wrong_ReportID");

        payload="";
        return payload;
    }
*/

    public static String WrongCustomerID_WrongReportNumber() throws IOException {
        PropertiescLass = readProperties.readProperties_AuditResultAPI();
        String reportnumber = PropertiescLass.get("wrong_ReportID");
        payload = "{\"report_id_in\":[\"" + reportnumber + "\"]}";
        return payload;

    }

    public static String postbulkReport_ReportIdIn_ReportIDNotIn() {
        //Workbench API
        List<String> reportlist_workbrnch = new ArrayList<String>();
        reportlist_workbrnch = reportlist.reportNumberFilter("reportlist");

        String reportNumber_IN = reportlist_workbrnch.get(0);

        String reportNumber_NotIN = reportlist_workbrnch.get(1);

        payload = "{\n" +
                "\t\"report_id_in\":[\""+reportNumber_IN+"\"],\n" +
                "\t\"report_id_not_in\":[\" "+reportNumber_NotIN+"\"]\n" +
                "\t\n" +
                "}";
        return payload;
    }

}
