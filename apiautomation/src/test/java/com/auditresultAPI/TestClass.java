package com.auditresultAPI;

import com.google.common.collect.Ordering;
import com.jayway.restassured.internal.support.Prettifier;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.utility.SpecBuilderClass;
import com.utility.logAttachment;
import com.utility.readProperties;
import com.workbenchflowAPI.emulateUser;
import com.workbenchflowAPI.getReportDetails;
import com.workbenchflowAPI.getReportNumberList;
import com.workbenchflowAPI.login;
import io.qameta.allure.Description;
import lombok.extern.java.Log;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log
public class TestClass {

    testFunctionality functionality = new testFunctionality();
    getReportNumberList reportlist = new getReportNumberList();

    static List<String> ReportNumberList = new ArrayList<>();

    List<String> AuditRulesName_Workbench = new ArrayList<String>();
    List<String> AuditRulesName_API = new ArrayList<String>();


    Map<String, String> AuditRulesName_Workbench_Map = new HashMap<>();
    Map<String, String> AuditRulesName_API_Map = new HashMap<>();

    static Map<String, String> PropertiescLass = new HashMap<String, String>();

    SpecBuilderClass specifications = new SpecBuilderClass();

    //Post Request Params
    List<String> postReportNumber = new ArrayList<String>();

    //Allure Attachment
    logAttachment attach = new logAttachment();

    //For pretty JSON
    Prettifier prettifier = new Prettifier();


    @BeforeSuite
    @Parameters({"company_id", "emulatedUser", "username", "password", "baseURI"})
    public void init(String company_id, String emulatedUser, String username, String password, String baseURI) {
        try {
            SpecBuilderClass.getDataFromFiles(company_id);
            SpecBuilderClass.initialization(company_id, emulatedUser, username, password, baseURI);
            login.init("Login");
            emulateUser.switchUser("SwitchUser");
            ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*   @Before
       public void setUp() {
           RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
       }
   */
    @Test(enabled = true)
    @Parameters("company_id")
    @Description("This method checks out the response of AuditResult API")
    public void getoneReport(String company_id) {

        log.info("***************************************Get Method oneReport********************************************************");

        //Audit Result API
        try {

            String reportnumber = ReportNumberList.get(0);
            Response response = functionality.oneReport(company_id, reportnumber, "getoneReport");

            if (response.statusCode() == 200) {
                log.info("Get API is working fine , with status code = " + response.statusCode());
                attach.APIresult("Get API is working fine , with status code = " + response.statusCode());
            } else {
                log.info("Get API is not working as expected , with status code = " + response.statusCode());
                attach.APIresult("Get API is not working as expected , with status code = " + response.statusCode());
            }

            attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));

            Assert.assertEquals(response.statusCode(), 200);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(enabled = true)
    @Parameters({"company_id"})
    @Description("It gives out result of risk level details at Header Level and Compares with Workbench API.")
    public void getoneReport_HeaderLevelAuditDetail(String company_id) {

        log.info("***************************************Get Method HeaderLevelAuditDetail********************************************************");

        try { String reportnumber = ReportNumberList.get(0);

            //Audit API
            Response response_AuditAPI = functionality.oneReport(company_id, reportnumber, "HeaderLevelAuditDetail");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            for (int i = 0, j = 0; i < 3; i++, j++) {
                AuditRulesName_API_Map.put(response_AuditAPI.body().jsonPath().getJsonObject("header_level_risk_details["+i+"].rule_name"),response_AuditAPI.body().jsonPath().getJsonObject("header_level_risk_details["+i+"].computed_risk_level"));
            }
            //Workbench API
            Response response_WorkbrnchAPI = getReportDetails.retrieveReportParameterFromFirstReport(reportnumber);

            for (int i = 0; i < 3; i++) {
                AuditRulesName_Workbench.add(response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.report_level_audit.rules[" + i + "].rule_name"));
            }

            if (AuditRulesName_Workbench.containsAll(AuditRulesName_API)) {
                log.info("Header level Risk Details Verified \nRule name are = " + AuditRulesName_Workbench);
                attach.APIresult("Header level Risk Details Verified \nRule name are = " + AuditRulesName_Workbench);

            } else {
                log.info("Header level Risk Details not matching");
                attach.APIresult("Header level Risk Details not matching");
            }

            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(AuditRulesName_API, AuditRulesName_Workbench);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test(enabled = true)
    @Parameters({"company_id"})
    @Description("It gives out result of risk level details at Line Level and Compares with Workbench API.")
    public void getoneReport_LineLevelAuditDetail(String company_id) {
        log.info("***************************************Get Method LineLevelAuditDetail********************************************************");

        ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
        String reportnumber = ReportNumberList.get(0);

        try {
            //Audit API

            Response response_AuditAPI = functionality.oneReport(company_id, reportnumber, "LineLevelAuditDetail");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            int arraysize_AuditAPI = jpath_AuditAPI.get("line_level_results[0].line_level_risk_details.size");
            for (int i = 0; i < arraysize_AuditAPI; i++) {
                AuditRulesName_API_Map.put(response_AuditAPI.body().jsonPath().getJsonObject("line_level_results[0].line_level_risk_details[" + i + "].rule_name"), response_AuditAPI.body().jsonPath().getJsonObject("line_level_results[0].line_level_risk_details[" + i + "].computed_risk_level"));
            }


            //Workbench API
            Response response_WorkbrnchAPI = getReportDetails.retrieveReportParameterFromFirstReport(reportnumber);
            JsonPath jpath_workbenchAPI = new JsonPath(response_WorkbrnchAPI.asString());
            int arraysize_workbench = jpath_workbenchAPI.get("data.expense_lines[0].rules.size");
            for (int i = 0; i < arraysize_workbench; i++) {
                int numberofRules = jpath_workbenchAPI.get("data.expense_lines[0].rules[" + i + "].rules.size");
                for (int j = 0; j < numberofRules; j++) {
                    AuditRulesName_Workbench_Map.put(response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.expense_lines[0].rules[" + i + "].rules[" + j + "].rule_name"), response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.expense_lines[0].rules[" + i + "].rules[" + j + "].risk_result.risk_level"));
                }

            }


            if (AuditRulesName_API_Map.equals(AuditRulesName_Workbench_Map)) {
                log.info("Checked for ReportNumber =" + reportnumber + "\nLine Level risk matching for Workbench API and Audit Result API\nLisk Level Details for Audit Result API" + AuditRulesName_API_Map + "\nLisk Level Details for Workbench API" + AuditRulesName_Workbench_Map);
                attach.APIresult("Checked for ReportNumber =" + reportnumber + "\nLine Level risk matching for Workbench API and Audit Result API\nLisk Level Details for Audit Result API" + AuditRulesName_API_Map + "\nLisk Level Details for Workbench API" + AuditRulesName_Workbench_Map);

            } else {
                log.info("Line Level risk NOT matching for Workbench API and Audit Result API");
                attach.APIresult("Line Level risk NOT matching for Workbench API and Audit Result API");
            }
            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(AuditRulesName_API_Map.equals(AuditRulesName_Workbench_Map), true);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(enabled = true)
    @Parameters({"company_id"})
    @Description("It checks if an Expense report is in 'Audit Pending' state , also compares result with workbench API")
    public void getoneReport_Auditpending(String company_id) {
        log.info("***************************************Get Method Auditpending********************************************************");

        try {
            // Audit Result API
            Response response_AuditAPI = functionality.oneReport(company_id, "010026435047", "Auditpending");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");

            if (response_AuditAPI.statusCode() == 500 & message.contains("is not audited yet.")) {
                log.info("Message for pending request = " + message + "\nStatusCode = " + response_AuditAPI.statusCode());
                attach.APIresult("Message for pending request = " + message + "\nStatusCode = " + response_AuditAPI.statusCode());
            } else {
                log.info("Message for pending request for get method is not working as expected");
                attach.APIresult("Message for pending request for get method is not working as expected");
            }
            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(message.contains("is not audited yet."), true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(enabled = true)
    @Parameters({"company_id"})
    @Description("It checks the response ,if Wrong Expense report number is provided.")
    public void getoneReport_WrongReportID(String company_id) {
        log.info("***************************************Get Method WrongReportID********************************************************");

        try {
            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            Object reportNumber = PropertiescLass.get("wrong_ReportID");

            Response response_AuditAPI = functionality.oneReport(company_id, (String) reportNumber, "WrongReportID");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");

            if (response_AuditAPI.statusCode() == 500 & message.contains("is not present")) {
                log.info("Message for Wrong ReportNumber in request = " + message + "\nIts working as expected.");
                attach.APIresult("Message for Wrong ReportNumber in request = " + message + "\nIts working as expected.");
            } else {
                log.info("Get call for 'Wrong Report ID' is not working fine");
                attach.APIresult("Get call for 'Wrong Report ID' is not working as expected");
            }

            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));

            Assert.assertEquals(message.contains("is not present"), true);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test(enabled = true)
    @Description("It checks the response ,if Wrong Expense Customer ID is provided.")
    public void getoneReport_WrongCustomerID() {
        log.info("***************************************Get Method Wrong CustomerID********************************************************");

        try {
            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            Object company_id = PropertiescLass.get("wrong_CustomerID");

            ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
            String reportnumber = ReportNumberList.get(0);

            Response response_AuditAPI = functionality.oneReport((String) company_id, reportnumber, "WrongCustomerID");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");

            if (response_AuditAPI.statusCode() == 500 & message.contains("is not present")) {
                log.info("Message for Wrong CustomerID in request = " + message + "\nStatusCode = " + response_AuditAPI.statusCode());
                attach.APIresult("Message for Wrong CustomerID in request = " + message + "\nStatusCode = " + response_AuditAPI.statusCode());
            } else {
                log.info("Response for Wrong Customer ID parmeter is not as expected.\nMessage in Response = " + message);
                attach.APIresult("Response for Wrong Customer ID parmeter is not as expected.\nMessage in Response = " + message);
            }
            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(message.contains("is not present"), true);
            Assert.assertEquals(response_AuditAPI.statusCode(), 500);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(enabled = true)
    @Parameters({"company_id"})
    @Description("It checks the response ,if Wrong API key is provided.")
    public void getoneReport_WrongAPIKey(String company_id) {
        log.info("***************************************Get Method WrongAPIKey********************************************************");

        try {
            ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
            String reportnumber = ReportNumberList.get(0);


            Response response_AuditAPI = functionality.oneReport(company_id, reportnumber, "WrongAPIKey");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("message");

            if (response_AuditAPI.statusCode() == 403 & message.contains("Forbidden")) {
                log.info("Message for Wrong x-api-key in request = " + message + "\nResponse is working as expected");
                attach.APIresult("Message for Wrong x-api-key in request = " + message + "\nResponse is working as expected");
            } else {
                log.info("Response of Wrong x-api-key is not working as expected.");
                attach.APIresult("Response of Wrong x-api-key is not working as expected.");
            }

            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(message.contains("Forbidden"), true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test(enabled = true)
    @Description("It checks the response, if combination of wrong CustomerID and Report number is provided")
    public void getoneReport_WrongCustomerID_WrongReportNumber() {
        log.info("***************************************Get Method Wrong CustomerID and WrongReportNumber********************************************************");

        try {
            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            String company_id = PropertiescLass.get("wrong_CustomerID");
            String reportnumber = PropertiescLass.get("wrong_ReportID");

            Response response_AuditAPI = functionality.oneReport(company_id, reportnumber, "WrongCustomerID_WrongReportNumber");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");

            if (response_AuditAPI.statusCode() == 500 & message.contains("is not present")) {
                log.info("Message for Combination of Wrong CustomerID and Wrong Report Number in request = " + message);
                attach.APIresult("Message for Combination of Wrong CustomerID and Wrong Report Number in request = " + message);
            } else {
                log.info("Combination of Wrong CustomerID and Wrong Report Number is not working as expected.\nMessage in Response = " + message);
                attach.APIresult("Combination of Wrong CustomerID and Wrong Report Number is not working as expected.\nMessage in Response = " + message);
            }

            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(response_AuditAPI.statusCode(), 500);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks the response for Post method call")
    public void postbulkReport(String company_id) {
        log.info("***************************************Post Method bulkReport********************************************************");

        try {
            boolean flag = false;
            int CountReport = 0;
            // Audit Result API
            Response ResultAuditAPI = functionality.bulkReport("default", company_id);
            JsonPath jpath = new JsonPath(ResultAuditAPI.asString());
            int reportSize = jpath.getJsonObject("content.size");
            for (int i = 0; i < reportSize; i++) {
                postReportNumber.add(jpath.getJsonObject("content[" + i + "].external_report_id"));
                //Workbench API
                Response WorkBenchAPI_response = getReportDetails.retrieveReportParameterFromFirstReport(postReportNumber.get(i));
                if (WorkBenchAPI_response.statusCode() == 200) {
                    CountReport++;
                    flag = true;
                } else {
                    flag = false;
                    break;
                }
            }


            if (flag == true) {
                log.info("Reports from Result Audit API is verified with Workbench reports \nTotal Reports Checked =" + CountReport + "\n" + postReportNumber);
                attach.APIresult("Reports from Result Audit API is verified with Workbench reports \nTotal Reports Checked =" + CountReport + "\n" + postReportNumber);

            } else {
                log.info("Reports from Result Audit API is not matching with Workbench reports");
                attach.APIresult("Reports from Result Audit API is not matching with Workbench reports");
            }

            attach.APIresponse("************* Response Header *************\n\n" + ResultAuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(ResultAuditAPI, ResultAuditAPI.getBody()));

            Assert.assertEquals(flag, true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks filter for report_id_not_in")
    public void postbulkReport_ReportIDNotin(String company_id) {
        log.info("***************************************Post Method ReportIdNotIN********************************************************");

        try {
            boolean flag = false;
            String reportNumber_AuditResultAPI = null;
            //Workbench API
            Response WorkbenchAPI = getReportDetails.retrieveReportParameterFromFirstReport(TestClass.ReportNumberList.get(0));
            JsonPath jpath_workbench = new JsonPath(WorkbenchAPI.asString());
            String reportNumber_Workbench = jpath_workbench.getJsonObject("data.expense_report.external_report_id");


            // Audit Result API
            Response ResultAuditAPI = functionality.bulkReport("ReportIDNotin", company_id);
            JsonPath jpath = new JsonPath(ResultAuditAPI.asString());
            int size_AuditResultAPI = jpath.getJsonObject("content.size");
            for (int i = 0; i < size_AuditResultAPI; i++) {
                reportNumber_AuditResultAPI = jpath.getJsonObject("content[" + i + "].external_report_id");

                if (reportNumber_AuditResultAPI.equals(reportNumber_Workbench)) {
                    flag = false;
                    break;
                } else
                    flag = true;
            }


            if (flag == false) {
                log.info("Verified , Audit result API is not working fine when payload is filtered for report_id_not_in");
                attach.APIresult("Verified , Audit result API is not working fine when payload is filtered for report_id_not_in");

            } else {
                log.info("Verified Audit Result's API response with Workbench API's response, its working fine.\nResponse Status Code " + ResultAuditAPI.statusCode());
                attach.APIresult("Verified Audit Result's API response with Workbench API's response, its working fine.\nResponse Status Code " + ResultAuditAPI.statusCode());

            }
            attach.APIresponse("************* Response Header *************\n\n" + ResultAuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(ResultAuditAPI, ResultAuditAPI.getBody()));


            Assert.assertEquals(flag, true);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks filter for Sort direction")
    public void postbulkReport_sortDirection(String company_id) {
        log.info("***************************************Post Method SortDirection********************************************************");

        List<String> ReportList_AuditResultAPI = new ArrayList<String>();
        // Audit Result API
        try {
            Response ResultAuditAPI = functionality.bulkReport("SortDirection", company_id);
            JsonPath jpath = new JsonPath(ResultAuditAPI.asString());
            int size = jpath.getJsonObject("content.size");
            for (int i = 0; i < size; i++) {
                ReportList_AuditResultAPI.add(jpath.getJsonObject("content[" + i + "].external_report_id"));
            }

            boolean desc_sorted = Ordering.natural().reverse().isOrdered(ReportList_AuditResultAPI);
            boolean aesc_sorted = Ordering.natural().isOrdered(ReportList_AuditResultAPI);

            if (desc_sorted == true) {
                log.info("Sort Direction,  for Descending Order is working \nValue is " + desc_sorted);
                attach.APIresult("Sort Direction,  for Descending Order is working \nValue is " + desc_sorted);

            } else {
                log.info("Sort Direction is not working as expected");
                attach.APIresult("Sort Direction is not working as expected");
            }
            attach.APIresponse("************* Response Header *************\n\n" + ResultAuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(ResultAuditAPI, ResultAuditAPI.getBody()));


            Assert.assertEquals(desc_sorted, true);

        } catch (IOException e) {
            e.fillInStackTrace();
        }


   /*     //Descending Logic
        boolean ascending = true, descending = true;
        for (int i = 1; i < ReportList_AuditResultAPI.size() && (ascending || descending); i++) {
            ascending = ascending && Integer.valueOf(ReportList_AuditResultAPI.get(i)) >=  Integer.valueOf(ReportList_AuditResultAPI.get(i-1));
            descending = descending && Integer.valueOf(ReportList_AuditResultAPI.get(i)) <=  Integer.valueOf(ReportList_AuditResultAPI.get(i-1));
        }

        System.out.println("Desc = "+descending);
        System.out.println("Aesc = "+ascending);
*/

    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks filter for Report Id in")
    public void postbulkReport_ReportIDin(String company_id) {
        log.info("***************************************Post Method ReportIdIN********************************************************");

        try {
            //Workbench API
            Response WorkbenchAPI = getReportDetails.retrieveReportParameterFromFirstReport(TestClass.ReportNumberList.get(0));
            JsonPath jpath_workbench = new JsonPath(WorkbenchAPI.asString());
            String reportNumber_Workbench = jpath_workbench.getJsonObject("data.expense_report.external_report_id");


            // Audit Result API
            Response ResultAuditAPI = functionality.bulkReport("ReportIDin", company_id);
            JsonPath jpath = new JsonPath(ResultAuditAPI.asString());
            String reportNumber_AuditResultAPI = jpath.getJsonObject("content[0].external_report_id");

            if (ResultAuditAPI.statusCode() == 200 & reportNumber_AuditResultAPI.equals(reportNumber_Workbench)) {
                log.info("Verified for report_id_in, compared Workbench reportNumber with Result Audit API\nStatuscode = " + ResultAuditAPI.statusCode());
                attach.APIresult("Verified for report_id_in, compared Workbench reportNumber with Result Audit API\nStatuscode = " + ResultAuditAPI.statusCode());

            } else {
                log.info("Workbench API and Audit Result API 's reportNumber doesnt match");
                attach.APIresult("Workbench API and Audit Result API 's reportNumber doesnt match");
            }

            attach.APIresponse("************* Response Header *************\n\n" + ResultAuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(ResultAuditAPI, ResultAuditAPI.getBody()));


            Assert.assertEquals(reportNumber_AuditResultAPI.equals(reportNumber_Workbench), true);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Test(enabled = true)
    @Parameters("company_id")
    @Description("checks filter for sort field")
    public void postbulkReport_SortField(String company_id) {
        log.info("***************************************Post Method SortField********************************************************");

        try {
            // Audit Result API
            Response AuditResultAPI = functionality.bulkReport("SortField", company_id);

            if (AuditResultAPI.statusCode() == 200) {
                log.info("Sort Field filter for Audit Result API is working fine");
                attach.APIresult("Sort Field filter for Audit Result API is working fine");

            } else {
                log.info("Sort Field filter for Audit Result API is not working as expected");
                attach.APIresult("Sort Field filter for Audit Result API is not working as expected");
            }


            attach.APIresponse("************* Response Header *************\n\n" + AuditResultAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(AuditResultAPI, AuditResultAPI.getBody()));


            Assert.assertEquals(AuditResultAPI.statusCode(), 200);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks filter for Page Number")
    public void postbulkReport_PageNumber(String company_id) throws Exception {
        log.info("***************************************Post Method PageNumber********************************************************");

        // Audit Result API

        Response AuditResultAPI = functionality.bulkReport("PageNumber", company_id);
        JsonPath jpath = new JsonPath(AuditResultAPI.asString());

        int pageNumber_AuditAPI = jpath.getJsonObject("pageable.pageNumber");
        if (AuditResultAPI.statusCode() == 200) {
            log.info("Page Number Filter is Working Fine");
            attach.APIresult("Page Number Filter is Working Fine");
        } else {
            log.info("Page Number Filter is not Working as expected");
            attach.APIresult("Page Number Filter is not Working as expected");

        }

        attach.APIresponse("************* Response Header *************\n\n" + AuditResultAPI.getHeaders() +
                "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(AuditResultAPI, AuditResultAPI.getBody()));


        Assert.assertEquals(AuditResultAPI.statusCode(), 200);


    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks filter for Page size")
    public void postbulkReport_PageSize(String company_id) throws Exception {
        log.info("***************************************Post Method PageSize********************************************************");

        // Audit Result API

        Response AuditResultAPI = functionality.bulkReport("PageSize", company_id);
        JsonPath jpath = new JsonPath(AuditResultAPI.asString());


        int pageNumber_AuditAPI = jpath.getJsonObject("pageable.pageSize");
        int numberofTotalReports = jpath.getJsonObject("content.size");

        if (AuditResultAPI.statusCode() == 200 & numberofTotalReports == 6) {
            log.info("Page Number Filter is Working Fine");
            attach.APIresult("Page Number Filter is Working Fine");
        } else {
            log.info("Page Number Filter is not Working as per expectation");
            attach.APIresult("Page Number Filter is not Working as per expectation");

        }
        attach.APIresponse("************* Response Header *************\n\n" + AuditResultAPI.getHeaders() +
                "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(AuditResultAPI, AuditResultAPI.getBody()));


        Assert.assertEquals(6, numberofTotalReports);

    }


    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks filter for Submission Filter")
    public void postbulkReport_submissionDatefilter(String company_id) {
        log.info("***************************************Post Method SubmissionDateFilter********************************************************");

        try {
            Response response = functionality.bulkReport("submissionfilter", company_id);

            if (response.statusCode() == 200) {
                log.info("Submission Date Filter works as expected");
                attach.APIresult("Submission Date Filter works as expected");
            } else {
                log.info("Submission Date Filter not working as expected");
                attach.APIresult("Submission Date Filter not working as expected");
            }

            attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));

            Assert.assertEquals(response.statusCode(), 200);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks for filter 'Audit Date filter'")
    public void postbulkReport_auditDatefilter(String company_id) {
        log.info("***************************************Post Method AuditDateFilter********************************************************");

        try {
            Response response = functionality.bulkReport("auditfilter", company_id);
            if (response.statusCode() == 200) {
                log.info("Audit Date Filter works as expected.");
                attach.APIresult("Audit Date Filter works as expected.");
            } else {
                log.info("Audit Date Filter not working as expected");
                attach.APIresult("Audit Date Filter not working as expected");
            }
            attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));


            Assert.assertEquals(response.statusCode(), 200);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(enabled = true)
    @Parameters({"company_id"})
    @Description("Checks for filter with 'Wrong Report ID'")
    public void postbulkReport_WrongReportID(String company_id) {
        log.info("***************************************Post Method WrongReportID********************************************************");

        try {
            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            Object reportNumber = PropertiescLass.get("wrong_ReportID");

            Response response_AuditAPI = functionality.bulkReport("WrongReportID", company_id);
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");

            if (response_AuditAPI.statusCode() == 500 & message.contains("with given filter have been found")) {
                log.info("Message for Wrong ReportNumber in request = " + message + "\nIts working as expected.");
                attach.APIresult("Message for Wrong ReportNumber in request = " + message + "\n Its working as expected.");
            } else {
                log.info("Post call for 'Wrong Report ID' is not working fine");
                attach.APIresult("Post call for 'Wrong Report ID' is not working as expected");

            }

            attach.APIresponse("************* Response Header *************\n\n" + response_AuditAPI.getHeaders() +
                    "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));

            Assert.assertEquals(message.contains("with given filter have been found"), true);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Test(enabled = true)
    @Description("Checks for filter with , 'Wrong Customer ID'")
    public void postbulkReport_WrongCustomerID() {
        log.info("***************************************Post Method WrongCustomerID********************************************************");

        try {

            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            Object wrong_CustomerID = PropertiescLass.get("wrong_CustomerID");

            Response response_AuditAPI = functionality.bulkReport("WrongCustomerID", (String) wrong_CustomerID);
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");


            if (response_AuditAPI.statusCode() == 500 & message.contains("with given filter have been found")) {
                log.info("Wrong CustomerID working fine\nMessage for Wrong CustomerID in request = " + message + "\nStatusCode = " + response_AuditAPI.statusCode());
            } else {
                log.info("Response for Wrong Customer ID paarmeter is not as expected.");
            }
            attach.APIresponse("************* Response Header *************\n\n"+response_AuditAPI.getHeaders()+
                    "\n\n********** Response Body************** \n"+prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(message.contains("with given filter have been found"), true);
            Assert.assertEquals(response_AuditAPI.statusCode(), 500);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

   /* @Test(enabled = true)
    @Parameters({"company_id"})
    public void postbulkReport_WrongAPIKey(String company_id) {
        try {
            ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
            String reportnumber = ReportNumberList.get(0);


            Response response_AuditAPI = functionality.oneReport(company_id, reportnumber, "WrongAPIKey");
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("message");
            Assert.assertEquals(message.contains("Forbidden"), true);

            if (response_AuditAPI.statusCode() == 403 & message.contains("Forbidden")) {
                log.info("Message for Wrong x-api-key in request = " + message);
            } else
                log.info("Response of Wrong x-api-key is not working as expected.");
        } catch (AssertionError e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
*/

    @Test(enabled = true)
    @Description("Checks for filter with combination of wrong 'CustomerID' and 'ReportNumber'")
    public void postbulkReport_WrongCustomerID_WrongReportNumber() {
        log.info("***************************************Post Method WrongCustomerID_WrongReportNumber********************************************************");

        try {
            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            String company_id = PropertiescLass.get("wrong_CustomerID");

            Response response_AuditAPI = functionality.bulkReport("WrongCustomerIDWrongReportNumber", company_id);
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            String message = jpath_AuditAPI.getJsonObject("msg");

            if (response_AuditAPI.statusCode() == 500 & message.contains("is not present")) {
                log.info("Combination of WrongCustomerID and WrongReportNumber is working Fine.\nMessage for Combination of Wrong CustomerID and Wrong Report Number in request = " + message);
                attach.APIresult("Combination of WrongCustomerID and WrongReportNumber is working Fine.\nMessage for Combination of Wrong CustomerID and Wrong Report Number in request = " + message);

            } else {
                log.info("Combination of Wrong CustomerID and Wrong Report Number is not working as expected");
                attach.APIresult("Combination of Wrong CustomerID and Wrong Report Number is not working as expected");
            }
            attach.APIresponse("************* Response Header *************\n\n"+response_AuditAPI.getHeaders()+
                    "\n\n********** Response Body************** \n"+prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(response_AuditAPI.statusCode(), 500);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Check for Line level risk audit details")
    public void postbulkReport_LineLevelAuditDetails(String company_id) {
        log.info("***************************************Post Method LineLevelAudit********************************************************");

        ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
        String reportnumber = ReportNumberList.get(0);

        try {
            //Audit API

            Response response_AuditAPI = functionality.bulkReport("LineLevelAuditDetail", company_id);
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            int arraysize_AuditAPI = jpath_AuditAPI.get("content[0].line_level_results[0].line_level_risk_details.size");
            System.out.println(arraysize_AuditAPI);
            for (int i = 0; i < arraysize_AuditAPI; i++) {
                AuditRulesName_API_Map.put(response_AuditAPI.body().jsonPath().getJsonObject("content[0].line_level_results[0].line_level_risk_details[" + i + "].rule_name"), response_AuditAPI.body().jsonPath().getJsonObject("content[0].line_level_results[0].line_level_risk_details[" + i + "].computed_risk_level"));
            }


            //Workbench API
            Response response_WorkbrnchAPI = getReportDetails.retrieveReportParameterFromFirstReport(reportnumber);
            JsonPath jpath_workbenchAPI = new JsonPath(response_WorkbrnchAPI.asString());
            int arraysize_workbench = jpath_workbenchAPI.get("data.expense_lines[0].rules.size");
            for (int i = 0; i < arraysize_workbench; i++) {
                int numberofRules = jpath_workbenchAPI.get("data.expense_lines[0].rules[" + i + "].rules.size");
                for (int j = 0; j < numberofRules; j++) {
                    AuditRulesName_Workbench_Map.put(response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.expense_lines[0].rules[" + i + "].rules[" + j + "].rule_name"), response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.expense_lines[0].rules[" + i + "].rules[" + j + "].risk_result.risk_level"));
                }

            }


            if (AuditRulesName_API_Map.equals(AuditRulesName_Workbench_Map)) {
                log.info("Checked for ReportNumber =" + reportnumber + "\nLine Level risk matching for Workbench API and Audit Result API\nLisk Level Details for Audit Result API" + AuditRulesName_API_Map + "\nLisk Level Details for Workbench API" + AuditRulesName_Workbench_Map);
                attach.APIresult("Checked for ReportNumber =" + reportnumber + "\nLine Level risk matching for Workbench API and Audit Result API\nLisk Level Details for Audit Result API" + AuditRulesName_API_Map + "\nLisk Level Details for Workbench API" + AuditRulesName_Workbench_Map);
            } else
            {       log.info("Line Level risk NOT matching for Workbench API and Audit Result API");
            attach.APIresult("Line Level risk NOT matching for Workbench API and Audit Result API");

        }

            attach.APIresponse("************* Response Header *************\n\n"+response_AuditAPI.getHeaders()+
                    "\n\n********** Response Body************** \n"+prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(AuditRulesName_API_Map.equals(AuditRulesName_Workbench_Map), true);



        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Check for Header level risk audit details")
    public void postbulkReport_HeadLevelAuditDetails(String company_id) {
        log.info("***************************************Post Method HeadLevelAudit********************************************************");

        ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
        String reportnumber = ReportNumberList.get(0);

        try {
            //Audit API

            Response response_AuditAPI = functionality.bulkReport("LineLevelAuditDetail", company_id);
            JsonPath jpath_AuditAPI = new JsonPath(response_AuditAPI.asString());
            for (int i = 0; i < 3; i++) {
                AuditRulesName_API_Map.put(response_AuditAPI.body().jsonPath().getJsonObject("content[0].header_level_risk_details[" + i + "].rule_name"), response_AuditAPI.body().jsonPath().getJsonObject("content[0].header_level_risk_details[" + i + "].computed_risk_level"));
            }


            //Workbench API
            Response response_WorkbrnchAPI = getReportDetails.retrieveReportParameterFromFirstReport(reportnumber);
            JsonPath jpath_workbenchAPI = new JsonPath(response_WorkbrnchAPI.asString());
            for (int i = 0; i < 3; i++) {
                AuditRulesName_Workbench_Map.put(response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.report_level_audit.rules[" + i + "].rule_name"), response_WorkbrnchAPI.body().jsonPath().getJsonObject("data.report_level_audit.rules[" + i + "].risk_result.risk_level"));
            }

            if (AuditRulesName_API_Map.equals(AuditRulesName_Workbench_Map)) {
                log.info("Checked for ReportNumber =" + reportnumber + "\nHeader Level risk matching for Workbench API and Audit Result API\nLisk Level Details for Audit Result API" + AuditRulesName_API_Map + "\nLisk Level Details for Workbench API" + AuditRulesName_Workbench_Map);
                attach.APIresult("Checked for ReportNumber =" + reportnumber + "\nHeader Level risk matching for Workbench API and Audit Result API\nLisk Level Details for Audit Result API" + AuditRulesName_API_Map + "\nLisk Level Details for Workbench API" + AuditRulesName_Workbench_Map);
            } else {
                log.info("Header Level risk NOT matching for Workbench API and Audit Result API");
                attach.APIresult("Header Level risk NOT matching for Workbench API and Audit Result API");
            }

            attach.APIresponse("************* Response Header *************\n\n"+response_AuditAPI.getHeaders()+
                    "\n\n********** Response Body************** \n"+prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));


            Assert.assertEquals(AuditRulesName_API_Map.equals(AuditRulesName_Workbench_Map), true);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Test(enabled = true)
    @Parameters("company_id")
    @Description("Checks for filter with combination of 'report_id_in' and 'report_id_not_in'")
    public void postbulkReport_ReportIdIn_ReportIDNotIn(String company_id) {
        log.info("***************************************Post Method ReportIDIn_ReportIDNotIN********************************************************");

        ReportNumberList = reportlist.reportNumberFilter("reportNumberFilter");
        String reportnumber_NotIN = ReportNumberList.get(1);

        boolean flag = true;

        try {
            //Audit API

            Response response_AuditAPI = functionality.bulkReport("ReportIdIn_ReportIDNotIn", company_id);
            JsonPath jpath = new JsonPath(response_AuditAPI.asString());
            int arraysize = jpath.getJsonObject("content.size");
            for (int i = 0; i < arraysize; i++) {
                if (jpath.getJsonObject("content[" + i + "].external_report_id").equals(reportnumber_NotIN)) {
                    flag = false;
                    log.info("Combination of ReportIdIn_ReportIDNotIn is not working as expected");
                    attach.APIresult("Combination of ReportIdIn_ReportIDNotIn is not working as expected");
                } else {
                    flag = true;
                    log.info("Combination of ReportIdIn_ReportIDNotIn is working as expected");
                    attach.APIresult("Combination of ReportIdIn_ReportIDNotIn is working as expected");
                }
            }

            attach.APIresponse("************* Response Header *************\n\n"+response_AuditAPI.getHeaders()+
                    "\n\n********** Response Body************** \n"+prettifier.getPrettifiedBodyIfPossible(response_AuditAPI, response_AuditAPI.getBody()));

            Assert.assertEquals(flag, true);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
