package com.auditresultAPI;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.utility.SpecBuilderClass;
import com.utility.readProperties;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


@Log
public class testFunctionality{

    static Map<String, String> PropertiescLass = new HashMap<String, String>();
    SpecBuilderClass specbuilder = new SpecBuilderClass();


    public Response oneReport(String company_id, String reportNumber, String UseCase) throws IOException {
        Response response;

        if (UseCase.contains("WrongCustomerID")) {
            response =
                    given().spec(SpecBuilderClass.requestSpecificationBuilder_AuditResultAPI())
                            .header("customer_id", company_id).
                            when()
                            .get("/customer/" + company_id + "/reports/" + reportNumber);
        }
        else if (UseCase.contains("WrongAPIKey")) {

            PropertiescLass = readProperties.readProperties_AuditResultAPI();
            Object wrong_x_api_key = PropertiescLass.get("wrong_x-api-key");

            response =
                    given().spec(SpecBuilderClass.requestSpecificationBuilder_AuditResultAPI())
                            .header("x-api-key", wrong_x_api_key).
                            when()
                            .get("/customer/" + company_id + "/reports/" + reportNumber);
        }
        else {
            response =
                    given().
                    spec(SpecBuilderClass.requestSpecificationBuilder_AuditResultAPI()).

                    when()
                            .get("/customer/" + company_id + "/reports/" + reportNumber);
        }

        return response;
    }


    public Response bulkReport(String UseCase, String company_id) throws IOException {

        String requestpayload = null;

        if (UseCase.equals("default") || UseCase.equals("WrongCustomerID"))
            requestpayload = "{}";

        else if(UseCase.equals("ReportIDin") ||UseCase.equals("WrongReportID") || UseCase.equals("LineLevelAuditDetail"))
            requestpayload= requestPayloads.reportINfilter(UseCase);

        else if(UseCase.equals("ReportIDNotin") )
            requestpayload=requestPayloads.reportNOTINfilter();

        else if(UseCase.equals("SortField"))
            requestpayload=requestPayloads.sortField();

        else if(UseCase.equals("SortDirection"))
            requestpayload=requestPayloads.sortDirection();

        else if(UseCase.equals("PageNumber"))
            requestpayload=requestPayloads.pageNumber();

        else if (UseCase.equals("PageSize"))
            requestpayload=requestPayloads.pageSize();

        else if( UseCase.equals("submissionfilter"))
            requestpayload=requestPayloads.submissionDatefilter();

        else if(UseCase.equals("auditfilter"))
            requestpayload= requestPayloads.auditDatefilter();

        else if(UseCase.equals("WrongCustomerIDWrongReportNumber"))
            requestpayload=  requestPayloads.WrongCustomerID_WrongReportNumber();

        else if(UseCase.equals("ReportIdIn_ReportIDNotIn")){
            requestpayload= requestPayloads.postbulkReport_ReportIdIn_ReportIDNotIn();
        }

        Response response =
                given()
                        .spec(SpecBuilderClass.requestSpecificationBuilder_AuditResultAPI())
                        .body(requestpayload).contentType(ContentType.JSON).
                        when()
                        .post("/customer/" + company_id + "/reports");

       return response;
    }


}

