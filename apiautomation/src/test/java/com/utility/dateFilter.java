//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.utility;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class dateFilter {
    public dateFilter() {
    }

    public String specificdate() {
        Date enddateutil = DateUtils.addDays(new Date(), -1);
        SimpleDateFormat sdf_end = new SimpleDateFormat("yyyy-MM-dd");
        String enddate = sdf_end.format(enddateutil);
        Date startdateutil = DateUtils.addDays(new Date(), -548);
        SimpleDateFormat sdf_start = new SimpleDateFormat("yyyy-MM-dd");
        String startdate = sdf_start.format(startdateutil);
        return "gte:" + startdate + ",lte:" + enddate + "";
    }

    public String randomdate() {
        int randomNum = ThreadLocalRandom.current().nextInt(20, 46);
        Date enddateutil = DateUtils.addDays(new Date(), -randomNum - 1);
        SimpleDateFormat sdf_end = new SimpleDateFormat("yyyy-MM-dd");
        String enddate = sdf_end.format(enddateutil);
        Date startdateutil = DateUtils.addDays(new Date(), -randomNum - 8);
        SimpleDateFormat sdf_start = new SimpleDateFormat("yyyy-MM-dd");
        String startdate = sdf_start.format(startdateutil);
        return "gte:" + startdate + ",lte:" + enddate + "";
    }
}
