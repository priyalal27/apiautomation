package com.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class readProperties {

    static Map<String, String> PropertiesFile_List = new HashMap<String, String>();

    public static Map<String, String> readProperties_AuditResultAPI() throws IOException {
        Properties prop = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("env.properties");
        prop.load(input);

        PropertiesFile_List.put("rcst", prop.getProperty("rcst"));
        PropertiesFile_List.put("x-api-key", prop.getProperty("x-api-key"));
        PropertiesFile_List.put("wrong_ReportID", prop.getProperty("wrong_ReportID"));
        PropertiesFile_List.put("wrong_CustomerID", prop.getProperty("wrong_CustomerID"));
        PropertiesFile_List.put("wrong_x-api-key", prop.getProperty("wrong_x-api-key"));


        return PropertiesFile_List;

    }

    public static Map<String, String> readProperties_AIngestionAPI() throws IOException {
        Properties prop = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("dbcredentials.properties");
        prop.load(input);

        PropertiesFile_List.put("url", prop.getProperty("url"));
        PropertiesFile_List.put("user", prop.getProperty("user"));
        PropertiesFile_List.put("password", prop.getProperty("password"));
        PropertiesFile_List.put("expense_reports_query",prop.getProperty("expense_reports_query"));

        return PropertiesFile_List;

    }
}

