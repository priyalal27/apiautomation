package com.utility;

import io.qameta.allure.Attachment;

public class logAttachment {

    @Attachment("Result")
    public static byte[] APIresult(String output) {
        String content = output;
        return content.getBytes();
    }

    @Attachment("API response")
    public static byte[] APIresponse(String output){
        String content = output;
        return content.getBytes();

    }


}
