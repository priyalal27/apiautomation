//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.utility;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import com.workbenchflowAPI.login;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SpecBuilderClass {
    static Object baseURI_ResultAPI;
    static Object x_api_key;
    static String company_id;
    static String emulatedUser;
    static String username;
    static String password;
    static String baseURI;
    static Map<String, String> PropertiescLass = new HashMap();

    public SpecBuilderClass() {
    }

    public static void initialization(String company_id, String emulatedUser, String username, String password, String baseURI) {
        SpecBuilderClass.company_id = company_id;
        SpecBuilderClass.emulatedUser = emulatedUser;
        SpecBuilderClass.username = username;
        SpecBuilderClass.password = password;
        SpecBuilderClass.baseURI = baseURI;
        System.out.println("Initialised Value =  " + baseURI_ResultAPI + " " + x_api_key + "  " + company_id + " " + emulatedUser + " " + username + " " + password + " " + baseURI + " ");
    }

    public static void getDataFromFiles(String company_id) throws IOException {
        PropertiescLass = readProperties.readProperties_AuditResultAPI();
        baseURI_ResultAPI = PropertiescLass.get("rcst");
        x_api_key = PropertiescLass.get("x-api-key");
    }

    public static RequestSpecification requestSpecificationBuilder_AuditResultAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.
                setBaseUri((String)baseURI_ResultAPI).
                addHeader("x-api-key", (String)x_api_key).
                addHeader("customer_id", company_id);
                //.addFilter((Filter) new AllureRestAssured());
        RequestSpecification requestSpec_AuditResultAPI = builder.build();
        return requestSpec_AuditResultAPI;
    }

    public static RequestSpecification responseSpecificationBuilder_AuditResultAPI() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        System.out.println(baseURI_ResultAPI + "" + x_api_key);
        RequestSpecification responseSpec_AuditResultAPI = builder.build();
        return responseSpec_AuditResultAPI;
    }

    public static RequestSpecification requestSpecificationBuilder_WorkbenchAPI(String Functionality) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        RequestSpecification requestSpec_WorkbenchAPI = null;
        if (Functionality.equals("Login")) {
            builder.setBaseUri(baseURI).addHeader("x-az-app-id", "1006").addHeader("Content-Type", "application/x-www-form-urlencoded").addFormParam("j_username", username).addFormParam("j_password", password);
            requestSpec_WorkbenchAPI = builder.build();
        } else if (Functionality.equals("SwitchUser")) {
            builder.setBaseUri(baseURI).
                    addCookie("JSESSIONID", login.jsessionidCookieValue).
                    addHeader("OWASP_CSRFTOKEN", login.authentication_token).
                    addHeader("X-Requested-With", "XMLHttpRequest").
                    addHeader("authorization", "Bearer " + login.jwtToken).addHeader("x-az-app-id", "1006").addPathParam("switchedUser", emulatedUser).addPathParam("customerId", company_id).setContent(ContentType.JSON);
            requestSpec_WorkbenchAPI = builder.build();
        } else if (Functionality.contains("other")) {
            requestSpec_WorkbenchAPI = builder.setBaseUri(baseURI).setContentType(ContentType.JSON).addCookie("JSESSIONID", login.jsessionidCookieValue).addHeader("OWASP_CSRFTOKEN", login.authentication_token).addHeader("authorization", "Bearer null").addHeader("X-Requested-With", "XMLHttpRequest").addHeader("x-az-app-id", "1006").build();
        }

        return requestSpec_WorkbenchAPI;
    }

    public void responseSpecificationBuilder() {
    }

    public static RequestSpecification otherrequest() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        RequestSpecification requestSpec_WorkbenchAPI = null;
        builder.setBaseUri(baseURI).addCookie("JSESSIONID", login.jsessionidCookieValue).addHeader("OWASP_CSRFTOKEN", login.authentication_token).addHeader("X-Requested-With", "XMLHttpRequest").addHeader("x-az-app-id", "1006").setContent(ContentType.JSON);
        requestSpec_WorkbenchAPI = builder.build();
        requestSpec_WorkbenchAPI = builder.build();
        return requestSpec_WorkbenchAPI;
    }
}
