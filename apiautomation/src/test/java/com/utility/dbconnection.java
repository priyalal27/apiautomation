package com.utility;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class dbconnection {

    static Connection con = null;
    static String url;
    static String user;
    static String password;
    static String reportNumber;


    //Read properties file
    static Map<String, String> dbcredential = new HashMap<String, String>();


    public static Connection getConnection() {

        url = dbcredential.get("url");
        user = dbcredential.get("user");
        password = dbcredential.get("password");
        try {

            con = DriverManager.getConnection(url, user, password);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return con;
    }


    public static String getResult(String reportnumber) throws IOException, SQLException {
        try {
            dbcredential = readProperties.readProperties_AIngestionAPI();
            con = getConnection();
            String query = dbcredential.get("expense_reports_query");

            System.out.println(query + "'" + reportnumber + "'");
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(query + "'" + reportnumber + "'");
            while (result.next()) {
                reportNumber = result.getString(6);
                System.out.println(reportNumber);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return reportNumber;
    }


}
