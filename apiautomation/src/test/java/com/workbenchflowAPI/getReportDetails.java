package com.workbenchflowAPI;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.utility.SpecBuilderClass;
import io.qameta.allure.Description;

import static com.jayway.restassured.RestAssured.given;

public class getReportDetails {

    static SpecBuilderClass specification = new SpecBuilderClass();

    @Description("Retrieve List of Expense Report")
    public static Response retrieveReportParameterFromFirstReport(String reportNumber) {



       Response response =
                given().
                        baseUri("https://console.rcst.dev.appzen.com/console/").
                        cookie("JSESSIONID", login.jsessionidCookieValue).
                        header("OWASP_CSRFTOKEN", login.authentication_token).
                        header("authorization","Bearer "+emulateUser.jwtToken).
                        header("X-Requested-With", "XMLHttpRequest").
                        header("x-az-app-id","1006").
                        contentType(ContentType.JSON).
                        get("/rest/v1/expense/reports/"+reportNumber);


/*
      Response   response =
                given().spec(SpecBuilderClass.requestSpecificationBuilder_WorkbenchAPI("other")).log().all()
                        .when()
                        .get("/rest/v1/expense/reports/"+reportNumber).then().log().all().extract().response();
//   .header("authorization","Bearer "+emulateUser.jwtToken)
*/
        return response;

    }

}
