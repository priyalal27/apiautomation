package com.workbenchflowAPI;

import com.jayway.restassured.response.Response;
import com.utility.SpecBuilderClass;
import io.qameta.allure.Description;
import lombok.extern.java.Log;

import static com.jayway.restassured.RestAssured.given;

@Log
public class emulateUser {

    static String jwtToken;
    static SpecBuilderClass specification = new SpecBuilderClass();

    @Description("Emulated User")
    public static void switchUser(String emulateusers) {
        Response response =
                given().spec(SpecBuilderClass.requestSpecificationBuilder_WorkbenchAPI(emulateusers))
                        .when()
                        .post("/rest/appZenSupportService/switchuser?userId={switchedUser}&customerId={customerId}");

        //JWT Token
        jwtToken = response.jsonPath().get("APPZEN_JWTTOKEN.token").toString();

    }
}
