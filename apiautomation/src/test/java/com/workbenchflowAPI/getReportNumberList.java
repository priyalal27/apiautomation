package com.workbenchflowAPI;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.utility.SpecBuilderClass;
import com.utility.dateFilter;
import org.json.simple.JSONObject;

import java.util.LinkedList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class getReportNumberList {

    dateFilter date = new dateFilter();
    static List<String> reportList = new LinkedList<>();
    SpecBuilderClass specification = new SpecBuilderClass();

    public List<String> reportNumberFilter(String reportsList) {


        JSONObject fullObject = new JSONObject();

        fullObject.put("audit_status", "AUTOMATED_AUDIT_APPROVED");
        fullObject.put("submission_date", date.specificdate());
        fullObject.put("offset", 0);
        fullObject.put("limit", 10);
        fullObject.put("response_format", "JSON");

        Response response =
                given().
                        baseUri("https://console.rcst.dev.appzen.com/console/").
                        cookie("JSESSIONID", login.jsessionidCookieValue).
                        header("OWASP_CSRFTOKEN", login.authentication_token).
                        header("authorization", "Bearer " + emulateUser.jwtToken).
                        header("X-Requested-With", "XMLHttpRequest").
                        header("x-az-app-id", "1006").
                        body(fullObject).
                        contentType(ContentType.JSON).
                        when()
                        .post("/rest/v1/expense/reports");

      /*Response response =
                given().spec(SpecBuilderClass.requestSpecificationBuilder_WorkbenchAPI("other"))
                        .log().all()
                        .body(fullObject)
                        .when()
                        .get("/rest/v1/expense/reports").then().log().all().extract().response();
*/
        String response_String = response.asString();
        JsonPath jpath = new JsonPath(response_String);

        int numberofreports = jpath.get("data.size");
        for (int i = 0; i < numberofreports; i++) {
            reportList.add(jpath.get("data[" + i + "].report_number"));
        }
        return reportList;

    }
}
