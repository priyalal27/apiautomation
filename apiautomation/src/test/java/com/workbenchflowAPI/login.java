package com.workbenchflowAPI;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.utility.SpecBuilderClass;
import io.qameta.allure.Description;
import lombok.extern.java.Log;

import static com.jayway.restassured.RestAssured.given;

/*
 *  This is a common class across all REST API tests. This class issues the REST API call to retrieve the following authentication parameters
 *  1) AWSALB cookie
 *  2) JSESSIONID cookie
 *  3) OWASP_CSRFTOKEN authentication token
 *
 *   These parameters are used for all subsequent REST API calls.
 *
 * */


@Log
public class login {

    public static String awsalbCookieValue;
    public static String jsessionidCookieValue;
    public static String authentication_token;
    public static String jwtToken;

    static SpecBuilderClass specification = new SpecBuilderClass();

    @Description("Login to AppZen")
    public static void init(String login) {

        RequestSpecification requestSpec = SpecBuilderClass.requestSpecificationBuilder_WorkbenchAPI(login);
        Response response = given().spec(requestSpec)
                .expect().statusCode(200)
                .when()
                .post("/j_spring_security_check");


        awsalbCookieValue = response.getCookie("AWSALB");
        jsessionidCookieValue = response.getCookie("JSESSIONID");
        authentication_token = response.jsonPath().getString("OWASP_CSRFTOKEN");
        jwtToken = response.jsonPath().get("APPZEN_JWTTOKEN.token").toString();
    }

}