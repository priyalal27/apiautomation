package com.ingestionAPI;



import com.jayway.restassured.internal.support.Prettifier;
import com.jayway.restassured.response.Response;
import com.utility.dbconnection;
import com.utility.logAttachment;
import io.qameta.allure.Description;
import lombok.extern.java.Log;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
@Log
public class testClass {
    ingestionfunctionality ingestion= new ingestionfunctionality();

    static int customerId;
    static String EmployeeID;
    static String emailAddress;
    static String orgId;
    static String reportnumber;
    static String dbReportNumber;

    //DB connction
    dbconnection database= new dbconnection();

    //Allure Report
    logAttachment attach = new logAttachment();


    //For pretty JSON
    Prettifier prettifier = new Prettifier();


    @BeforeClass
    @Parameters({"customerId","EmployeeID","emailAddress","orgId"})
    public void payloadUpdate(int customerId,String EmployeeID, String emailAddress, String orgId){
        this.customerId=customerId;
        this.EmployeeID=EmployeeID;
        this.emailAddress = emailAddress;
        this.orgId=orgId;

    }

    @Test(priority = 0)
    @Description("New report is ingested when Valid Email Address is given in UserID field")
    public void UserID_ValidEmailAddress() throws IOException, ParseException, InterruptedException, SQLException {
        reportnumber=ingestionfunctionality.reportNumberGenerator();
        Response response=ingestion.IngestionPayloadAPI("UserID_ValidEmailAddress", reportnumber);
        String result=(String) response.getBody().jsonPath().get("Status");

        if(result.matches("Success")) {
            Thread.sleep(30000);
            dbReportNumber = database.getResult(reportnumber);
            if (dbReportNumber.equals(reportnumber))
            {
                attach.APIresult("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
                log.info("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
        }
            else {
                log.info("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
                attach.APIresult("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
            }
        }

        else
        {
            attach.APIresult("***Report is not ingested***");
            log.info("***Report is not ingested***");

        }

        attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));

        Assert.assertEquals(response.statusCode(),200);


    }

    @Test(priority = 1)
    @Description("New report is ingested when valid EmployeeID is given in UserID field")
    public void UserID_ValidEmployeeID() throws IOException, ParseException, InterruptedException, SQLException {
        reportnumber=ingestionfunctionality.reportNumberGenerator();
        Response response=ingestion.IngestionPayloadAPI("UserID_ValidEmployeeID", reportnumber);

        String result=(String) response.getBody().jsonPath().get("Status");

        if(result.matches("Success")) {
            Thread.sleep(30000);
            dbReportNumber = database.getResult(reportnumber);
            if (dbReportNumber.equals(reportnumber))
            {
                attach.APIresult("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
                log.info("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
            }
            else {
                log.info("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
                attach.APIresult("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
            }
        }

        else
        {
            attach.APIresult("***Report is not ingested***");
            log.info("***Report is not ingested***");
        }



        attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));


        Assert.assertEquals(response.statusCode(),200);
    }

    @Test(priority = 2)
    @Description("New report is ingested when valid EmailAddress is given in Email Address field")
    public void EmailID_ValidEmailAddress() throws IOException, ParseException, InterruptedException, SQLException {
        reportnumber=ingestionfunctionality.reportNumberGenerator();
       Response response = ingestion.IngestionPayloadAPI("EmailID_ValidEmailAddress",reportnumber);

        String result=(String) response.getBody().jsonPath().get("Status");

        if(result.matches("Success")) {
            Thread.sleep(30000);
            dbReportNumber = database.getResult(reportnumber);
            if (dbReportNumber.equals(reportnumber))
            {
                attach.APIresult("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
                log.info("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
            }
            else {
                log.info("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
                attach.APIresult("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
            }
        }

        else
        {
            attach.APIresult("***Report is not ingested***");
            log.info("***Report is not ingested***");
        }


        attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));

        Assert.assertEquals(response.statusCode(),200);

    }

    @Test(priority = 3)
    @Description("New report is ingested when both EmailID and Employee ID are invalid.")
    public void InvalidEmailID_InvalidEmployeeID() throws IOException, ParseException, InterruptedException, SQLException {
        reportnumber=ingestionfunctionality.reportNumberGenerator();
        Response response =ingestion.IngestionPayloadAPI("InvalidEmailID_InvalidEmployeeID",reportnumber);
        String result=(String) response.getBody().jsonPath().get("Status");

        if(result.matches("Success")) {
            Thread.sleep(30000);
            dbReportNumber = database.getResult(reportnumber);
            if (dbReportNumber.equals(reportnumber))
            {
                attach.APIresult("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
                log.info("***Report successfully Ingested through API ****\n\"Report Ingested = " + reportnumber + "" + "\n And also report is added in database");
            }
            else {
                log.info("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
                attach.APIresult("***Report successfully Ingested through API****\n\"Report Ingested = " + reportnumber + "" + "\n Report not added in database");
            }
        }

        else
        {
            attach.APIresult("***Report is not ingested***");
            log.info("***Report is not ingested***");
        }



        attach.APIresponse("************* Response Header *************\n\n" + response.getHeaders() +
                "\n\n********** Response Body************** \n" + prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));

        Assert.assertEquals(response.statusCode(),200);

    }


}
