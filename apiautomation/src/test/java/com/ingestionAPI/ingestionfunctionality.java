package com.ingestionAPI;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.utility.dbconnection;
import lombok.extern.java.Log;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

@Log
public class ingestionfunctionality {

    static String reportnumber= null;

    public Response IngestionPayloadAPI(String UseCase, String Reportnumber) throws IOException, ParseException, InterruptedException, SQLException {
        String request_payload = null;

        if(UseCase.equals("UserID_ValidEmailAddress"))
            request_payload = requestPayload.UserID_validEmailAddress();

        else if(UseCase.equals("UserID_ValidEmployeeID"))
            request_payload= requestPayload.UserID_validEmployeeID();

        else if(UseCase.equals("EmailID_ValidEmailAddress"))
            request_payload= requestPayload.EmailID_ValidEmailAddress();

        else if(UseCase.equals("InvalidEmailID_InvalidEmployeeID"))
            request_payload= requestPayload.InvalidEmailID_InvalidEmployeeID();

        RestAssured.baseURI="http://ingestion-expense-pub.rcst.dev.appzen.com/";


        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .header("customer_id", testClass.customerId)
                        .header("x-api-key", "0ohfnc9fDLWwsAawjYr4Ps-Af37DSerlx")
                        .body(request_payload)

                        .when()
                        .put("reports/"+reportnumber)
                        .then()
                        .assertThat().
                        body("Status",equalTo("Success")).
                        extract().response();

        return response;
    }

    public static String reportNumberGenerator(){
        Random random=new Random();
        int num=random.nextInt(1000) ;
        reportnumber="Testing12"+String.valueOf(num);
        return reportnumber;
    }

}
